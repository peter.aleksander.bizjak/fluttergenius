import 'dart:convert';
import 'dart:io';

import 'package:fluttergenius/utils/constants/api.consts.dart';
import 'package:fluttergenius/utils/exceptions/network.exception.dart';
import 'package:fluttergenius/utils/helpers/config.helper.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class ApiProvider {
  Future<Map<String, dynamic>> getRequest(
    String endpoint, {
    Map<String, String> queryParams,
  }) async {
    final String apiKey = await _retrieveApiToken();
    final Response response = await http.get(
      Uri.https(BASE_API_URL, endpoint, queryParams),
      headers: {HttpHeaders.authorizationHeader: 'Bearer $apiKey'},
    );
    if (response.statusCode != 200) {
      throw ApiException('Response did not return 200: ${response.body}');
    }
    return json.decode(response.body) as Map<String, dynamic>;
  }

  Future<String> _retrieveApiToken() async {
    return (await loadConfigFile())['genius_access_token'] as String;
  }
}
