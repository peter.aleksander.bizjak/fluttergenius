import 'package:fluttergenius/api/models/search.item.model.dart';
import 'package:fluttergenius/api/models/song.details.model.dart';
import 'package:fluttergenius/api/providers/api.provider.dart';
import 'package:fluttergenius/api/repositories/iapi.repository.dart';
import 'package:fluttergenius/utils/exceptions/network.exception.dart';

class ApiRepository implements IApiRepository {
  static final ApiRepository instance = ApiRepository._();

  final ApiProvider _provider = ApiProvider();

  ApiRepository._();

  @override
  Future<List<SearchItemModel>> performWebSearch(String searchQuery) async {
    try {
      return ((await _provider.getRequest(
        'search',
        queryParams: {'q': searchQuery},
      ))['response']['hits'] as List<dynamic>)
          .map<SearchItemModel>((hit) => SearchItemModel.fromJson(hit))
          .toList(growable: false);
    } on ApiException catch (apiException) {
      print(apiException.message);
      return null;
    } catch (exception) {
      print(exception.toString());
      return null;
    }
  }

  @override
  Future<SongDetailsModel> retrieveSongDetails(int songID) async {
    try {
      return SongDetailsModel.fromJson(
        await _provider.getRequest(
          'songs/$songID',
          queryParams: {'text_format': 'plain'},
        ),
      );
    } on ApiException catch (apiException) {
      print(apiException.message);
      return null;
    } catch (exception) {
      print(exception.toString());
      return null;
    }
  }
}
