import 'package:fluttergenius/api/models/search.item.model.dart';
import 'package:fluttergenius/api/models/song.details.model.dart';

abstract class IApiRepository {
  Future<List<SearchItemModel>> performWebSearch(String searchQuery);
  Future<SongDetailsModel> retrieveSongDetails(int songID);
}
