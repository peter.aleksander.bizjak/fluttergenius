class SearchItemModel {
  int id;
  String title;
  String artist;
  String thumbnailUrl;

  SearchItemModel.fromJson(Map<String, dynamic> json) {
    id = json['result']['id'];
    title = json['result']['full_title'];
    artist = json['result']['primary_artist']['name'];
    thumbnailUrl = json['result']['header_image_thumbnail_url'];
  }
}
