class SongDetailsModel {
  int id;
  String title;
  String thumbnailURL;
  String album;
  String media;
  String lyrics;
  String description;

  SongDetailsModel.fromJson(Map<String, dynamic> json) {
    id = json['response']['song']['id'];
    title = json['response']['song']['title'];
    thumbnailURL = json['response']['song']['header_image_thumbnail_url'];
    album = json['response']['song']['album']['full_title'];
    media = json['response']['song']['media'][0]['url'];
    lyrics = json['response']['song']['url'];
    description = json['response']['song']['description']['plain'];
  }
}
