import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttergenius/blocs/details/details.dart';
import 'package:fluttergenius/blocs/search/search.dart';
import 'package:fluttergenius/ui/app.dart';

void main() {
  runApp(
    MultiBlocProvider(
      providers: <BlocProvider>[
        BlocProvider<SearchBloc>(
          create: (BuildContext context) => SearchBloc(),
        ),
        BlocProvider<DetailsBloc>(
          create: (BuildContext context) => DetailsBloc(),
        )
      ],
      child: App(),
    ),
  );
}
