import 'package:flutter/material.dart';

const Color COLOR_PRIMARY = Color(0xFFA3C3D9);
const Color COLOR_ACCENT = Color(0xFFAE76A6);
const Color COLOR_BACKGROUND = Color(0xFFFDFFFC);
