import 'package:fluttergenius/api/models/song.details.model.dart';

class DetailsState {
  const DetailsState();
}

class EmptyDetailsState extends DetailsState {}

class LoadingDetailsState extends DetailsState {}

class SuccessfulDetailsState extends DetailsState {
  final SongDetailsModel data;
  const SuccessfulDetailsState(this.data);
}

class FailedDetailsState extends DetailsState {
  final String error;
  const FailedDetailsState(this.error);
}
