import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttergenius/api/repositories/api.repository.dart';
import 'package:fluttergenius/blocs/details/details.dart';

class DetailsBloc extends Bloc<DetailsEvent, DetailsState> {
  final ApiRepository _apiRepository = ApiRepository.instance;

  DetailsBloc() : super(EmptyDetailsState());

  @override
  Stream<DetailsState> mapEventToState(DetailsEvent event) async* {
    if (event is RequestDetailsEvent) {
      yield LoadingDetailsState();
      if (event.songID > 0) {
        final result = await _apiRepository.retrieveSongDetails(event.songID);
        if (result != null) {
          yield SuccessfulDetailsState(result);
        } else {
          yield FailedDetailsState('Search failed for ${event.songID}');
        }
      } else {
        yield EmptyDetailsState();
      }
    } else {
      yield FailedDetailsState('Unknown event: ${event.toString()}');
    }
  }
}
