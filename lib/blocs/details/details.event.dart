abstract class DetailsEvent {
  const DetailsEvent();
}

class RequestDetailsEvent extends DetailsEvent {
  final int songID;
  const RequestDetailsEvent(this.songID);
}
