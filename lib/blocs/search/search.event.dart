abstract class SearchEvent {
  const SearchEvent();
}

class TextChangedSearchEvent extends SearchEvent {
  final String searchQuery;
  const TextChangedSearchEvent(this.searchQuery);
}
