import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttergenius/api/repositories/api.repository.dart';
import 'package:fluttergenius/blocs/search/search.event.dart';
import 'package:fluttergenius/blocs/search/search.state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final ApiRepository _apiRepository = ApiRepository.instance;

  SearchBloc() : super(EmptySearchState());

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is TextChangedSearchEvent) {
      yield LoadingSearchState();
      if (event.searchQuery.isEmpty) {
        yield EmptySearchState();
      } else {
        final result = await _apiRepository.performWebSearch(event.searchQuery);
        if (result != null) {
          yield SuccessfulSearchState(result);
        } else {
          yield FailedSearchState('Search failed for: ${event.searchQuery}');
        }
      }
    } else {
      yield FailedSearchState('Unknown event: ${event.toString()}');
    }
  }
}
