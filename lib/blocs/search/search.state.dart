import 'package:fluttergenius/api/models/search.item.model.dart';

abstract class SearchState {
  const SearchState();
}

class EmptySearchState extends SearchState {}

class LoadingSearchState extends SearchState {}

class SuccessfulSearchState extends SearchState {
  final List<SearchItemModel> data;
  const SuccessfulSearchState(this.data);
}

class FailedSearchState extends SearchState {
  final String error;
  const FailedSearchState(this.error);
}
