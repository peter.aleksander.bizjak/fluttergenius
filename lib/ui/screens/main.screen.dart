import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttergenius/api/models/search.item.model.dart';
import 'package:fluttergenius/blocs/search/search.dart';
import 'package:fluttergenius/ui/screens/details.screen.dart';

class HomeScreen extends StatefulWidget {
  static const String ROUTE_NAME = '/';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController _controller;
  bool _searchEnabled = false;

  SearchBloc _blocProvider;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    _blocProvider.close();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _blocProvider = BlocProvider.of<SearchBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: _searchEnabled
            ? TextFormField(
                cursorColor: Colors.black38,
                keyboardType: TextInputType.text,
                controller: _controller,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  contentPadding: const EdgeInsets.all(16.0),
                  hintText: 'Search',
                  suffixIcon: IconButton(
                    icon: Icon(Icons.done),
                    onPressed: () {
                      FocusScope.of(context).unfocus();
                      _blocProvider.add(
                        TextChangedSearchEvent(_controller.text),
                      );
                    },
                  ),
                ),
              )
            : Text('FlutterGenius'),
        centerTitle: !_searchEnabled,
        actions: [
          IconButton(
            icon: _searchEnabled ? Icon(Icons.clear) : Icon(Icons.search),
            onPressed: () => setState(() => _searchEnabled = !_searchEnabled),
          ),
        ],
      ),
      body: SafeArea(
        minimum: const EdgeInsets.all(16.0),
        child: BlocBuilder<SearchBloc, SearchState>(
          builder: (_, SearchState state) {
            if (state is EmptySearchState) {
              return Container();
            } else if (state is LoadingSearchState) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if (state is SuccessfulSearchState) {
              return ListView.separated(
                padding: EdgeInsets.zero,
                itemBuilder: (BuildContext listContext, int index) {
                  final SearchItemModel result = state.data[index];
                  return ListTile(
                    leading: Image(
                      width: 56.0,
                      height: 56.0,
                      fit: BoxFit.fill,
                      image: NetworkImage(result.thumbnailUrl),
                    ),
                    title: Text(result.title),
                    subtitle: Text(result.artist),
                    onTap: () {
                      Navigator.of(listContext).push(
                        MaterialPageRoute(
                          settings: RouteSettings(
                            name: DetailsScreen.ROUTE_NAME,
                          ),
                          builder: (BuildContext cntx) => DetailsScreen(
                            songID: result.id,
                          ),
                        ),
                      );
                    },
                  );
                },
                separatorBuilder: (_, int index) => SafeArea(
                  child: SizedBox(
                    height: 8.0,
                  ),
                ),
                itemCount: state.data.length,
              );
            } else if (state is FailedSearchState) {
              return Center(
                child: Text(state.error),
              );
            } else {
              return Center(
                child: Text('Application enterned unknown state'),
              );
            }
          },
        ),
      ),
    );
  }
}
