import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttergenius/blocs/details/details.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailsScreen extends StatefulWidget {
  static const String ROUTE_NAME = '/details';
  final int songID;

  DetailsScreen({@required this.songID});

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  DetailsBloc _detailsBloc;

  @override
  void dispose() {
    _detailsBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _detailsBloc = BlocProvider.of<DetailsBloc>(context)
      ..add(
        RequestDetailsEvent(
          widget.songID,
        ),
      );
    return Scaffold(
      body: BlocBuilder<DetailsBloc, DetailsState>(
        builder: (BuildContext buildContext, DetailsState state) {
          if (state is EmptyDetailsState || state is LoadingDetailsState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is SuccessfulDetailsState) {
            return NestedScrollView(
              headerSliverBuilder: (BuildContext headerCntx, bool scrolled) {
                return <Widget>[
                  SliverAppBar(
                    expandedHeight: 200.0,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      background: Image.network(
                        state.data.thumbnailURL,
                        fit: BoxFit.cover,
                      ),
                    ),
                  )
                ];
              },
              body: ListView(
                padding: EdgeInsets.zero,
                children: [
                  ListTile(
                    title: Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: Text(
                        '${state.data.title} (${state.data.album})',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    subtitle: Text('Listen online'),
                    onTap: () async {
                      if (await canLaunch(state.data.media)) {
                        await launch(state.data.media);
                      }
                    },
                  ),
                  ListTile(
                    title: Text('Lyrics'),
                    subtitle: Text(state.data.lyrics),
                    onTap: () async {
                      if (await canLaunch(state.data.lyrics)) {
                        await launch(state.data.lyrics);
                      }
                    },
                  ),
                  ListTile(
                    title: Padding(
                        padding: const EdgeInsets.only(
                          bottom: 8.0,
                        ),
                        child: Text('About')),
                    subtitle: Text(state.data.description),
                  ),
                ],
              ),
            );
          } else if (state is FailedDetailsState) {
            return Center(
              child: Text(state.error),
            );
          } else {
            return Center(
              child: Text('Application has entered unknown state'),
            );
          }
        },
      ),
    );
  }
}
