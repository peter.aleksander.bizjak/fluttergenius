import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttergenius/ui/screens/main.screen.dart';
import 'package:fluttergenius/utils/constants/color.consts.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: COLOR_BACKGROUND,
      ),
    );
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: COLOR_PRIMARY,
        accentColor: COLOR_ACCENT,
        scaffoldBackgroundColor: COLOR_BACKGROUND,
        appBarTheme: AppBarTheme(
          color: COLOR_BACKGROUND,
          elevation: 0.0,
        ),
      ),
      home: HomeScreen(),
    );
  }
}
